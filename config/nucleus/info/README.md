# Collegi Pixelmon Server #
## Info Directory ##

### Description ###
These files are the source files for what appears on screen in the game when a
player uses the */info* command. To correct typos you can either create a
merge request against this repository, or you can use the normal Google Forms
bug reporting method.

### Notes ###
The *colors.txt* file is not actually synced with the server. It exists to allow
editors to see how they can go about adding colors to their info pages. Please
do not submit merge requests that change this file. Thanks!
