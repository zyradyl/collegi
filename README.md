# Collegi Pixelmon Server #
## Primary Repository ##

### Description ###
In addition to serving as an issue tracker for server wide issues, this also
serves as a place where different plain text files are made available for our
users and staff members to look over and edit if they choose to do so.

Each group of files is divided into a directory that includes a README that
explains what that directory's files are used for. Staff or Users that wish
to submit a change to those files can submit a merge request through GitLab
with an explanation of their changes.

Please note that this is a new process, so we haven't been able to completely
flesh it out yet. If you notice any problems, and you don't know how or do not
want to create a Merge Request to fix the issue, please report it as a standard
bug through the
[Google Forms Bug Report](https://goo.gl/forms/PEJNZsVx9rSLGu6F2) form.

Thank you for your time! This will likely expand with more information as time
goes on.
